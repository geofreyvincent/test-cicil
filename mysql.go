package main

import (
	"database/sql"
	"log"
)

func connect() *sql.DB {
	db, err := sql.Open("mysql", "root:Vincent581@tcp(localhost:3306)/test_cicil")

	if err != nil {
		log.Fatal(err)
	}

	return db
}
