package main

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

type listPhones struct {
	Firstname   string `json:"firstname" validate:"required"`
	Lastname    string `json:"lastname" validate:"required"`
	PhoneNumber string `json:"phone_number" validate:"required,numeric"`
	Address     string `json:"address" validate:"required"`
	ID          string `json:"id"`
}

// e.GET("/phones", getPhones)
func getPhones(c echo.Context) error {

	result := []map[string]interface{}{}
	db := connect()
	defer db.Close()

	phones, err := db.Query(`SELECT firstname,lastname,phone_number,address FROM phonebook WHERE IFNULL(deleted_by,'') = ''`)
	//if error return 500 can't connect to mysql
	if err != nil {
		return c.JSON(500, map[string]interface{}{
			"code": 500,
			"data": "",
			"msg":  "INTERNAL SERVER ERROR",
		})
	}
	defer phones.Close()
	for phones.Next() {
		phone := listPhones{}
		err := phones.Scan(&phone.Firstname, &phone.Lastname, &phone.PhoneNumber, &phone.Address)
		//if error return 404 no data
		if err != nil {
			return c.JSON(404, map[string]interface{}{
				"code": 404,
				"data": "",
				"msg":  "Data tidak ditemukan",
			})
		}
		result = append(result, map[string]interface{}{
			"firstname":    phone.Firstname,
			"lastname":     phone.Lastname,
			"phone_number": phone.PhoneNumber,
			"address":      phone.Address,
		})
	}

	return c.JSON(200, map[string]interface{}{"code": "200", "data": result, "msg": "success"})
}

// e.GET("/phone/:id", getPhone)
func getPhone(c echo.Context) error {
	id := c.Param("id")
	phone := listPhones{}
	db := connect()
	defer db.Close()

	err := db.QueryRow(`SELECT firstname,lastname,phone_number,address FROM phonebook WHERE id = ? AND IFNULL(deleted_by,'') = ''`, id).
		Scan(&phone.Firstname, &phone.Lastname, &phone.PhoneNumber, &phone.Address)
	//if error return 500 can't connect to mysql
	if err != nil {
		return c.JSON(500, map[string]interface{}{"code": 500, "msg": "Internal Server Error"})
	}
	data := map[string]interface{}{
		"firstname":    phone.Firstname,
		"lastname":     phone.Lastname,
		"phone_number": phone.PhoneNumber,
		"address":      phone.Address,
	}
	return c.JSON(200, map[string]interface{}{"code": "200", "data": data, "msg": "success"})
}

// e.POST("/phone", savePhone)
func savePhone(c echo.Context) error {

	phone := new(listPhones)
	checkPhone := new(listPhones)
	//binding with json data and struct data
	if err := c.Bind(phone); err != nil {
		return err
	}
	//after bind validate using custom validator
	if err := c.Validate(phone); err != nil {
		return err
	}
	db := connect()
	defer db.Close()

	err := db.QueryRow(`SELECT address FROM phonebook WHERE address = ? AND IFNULL(deleted_by,'') = ''`, phone.Address).
		Scan(&checkPhone.Address)
	//check kalo address sudah ada tidak bisa digunakan ulang untuk phonebook
	if len(checkPhone.Address) != 0 {
		return c.JSON(404, map[string]interface{}{"code": 404, "msg": "Address sudah digunakan"})
	}
	sql := "INSERT INTO phonebook(firstname, lastname, phone_number, address, created_at, created_by) VALUES( ?, ?, ?, ?, now(), 1)"
	stmt, err := db.Prepare(sql)

	if err != nil {
		fmt.Print(err.Error())
	}
	defer stmt.Close()
	result, err2 := stmt.Exec(phone.Firstname, phone.Lastname, phone.PhoneNumber, phone.Address)

	// Exit if we get an error
	if err2 != nil {
		return c.JSON(500, map[string]interface{}{"code": 500, "msg": "Internal Server Error"})
	}
	return c.JSON(200, map[string]interface{}{"code": "200", "data": result, "msg": "insert data success"})
}

// e.POST("/update_phone", updatePhone)
func updatePhone(c echo.Context) error {

	phone := new(listPhones)
	//binding with json data and struct data
	if err := c.Bind(phone); err != nil {
		return err
	}
	db := connect()
	defer db.Close()

	sql := "UPDATE phonebook SET firstname = ?, lastname = ?, phone_number = ? WHERE id = ?"
	stmt, err := db.Prepare(sql)

	if err != nil {
		fmt.Print(err.Error())
	}
	defer stmt.Close()
	result, err2 := stmt.Exec(phone.Firstname, phone.Lastname, phone.PhoneNumber, phone.ID)

	// Exit if we get an error
	if err2 != nil {
		return c.JSON(500, map[string]interface{}{"code": 500, "msg": "Internal Server Error"})
	}
	return c.JSON(200, map[string]interface{}{"code": "200", "data": result, "msg": "update data success"})
}

// e.POST("/delete_phone", deletePhone)
func deletePhone(c echo.Context) error {

	phone := new(listPhones)
	//binding with json data and struct data
	if err := c.Bind(phone); err != nil {
		return err
	}
	db := connect()
	defer db.Close()

	sql := "UPDATE phonebook SET deleted_at = NOW(), deleted_by = 1 WHERE id = ?"
	stmt, err := db.Prepare(sql)

	if err != nil {
		fmt.Print(err.Error())
	}
	defer stmt.Close()
	result, err2 := stmt.Exec(phone.ID)

	// Exit if we get an error
	if err2 != nil {
		return c.JSON(500, map[string]interface{}{"code": 500, "msg": "Internal Server Error"})
	}
	return c.JSON(200, map[string]interface{}{"code": "200", "data": result, "msg": "delete data success"})
}
